# -- Version plus axée déploiement (running on nginx)

# FROM node:21.6.2-alpine AS build
# WORKDIR /app
# COPY package*.json ./
# RUN npm install -g @quasar/cli -y
# COPY . .
# RUN npm install -y
# RUN quasar build

# FROM nginx:alpine3.18
# COPY --from=build /app/dist/spa /usr/share/nginx/html
# EXPOSE 80

# CMD ["nginx", "-g", "daemon off;"]


# --- Version avec Quasar en mode dev directement

FROM node:21.6.2-alpine
WORKDIR /app
COPY . .
RUN npm install -g @quasar/cli
RUN npm install
CMD ["quasar", "dev"]
