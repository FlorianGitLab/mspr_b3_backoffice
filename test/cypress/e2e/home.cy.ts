describe('e2e  tests', () => {
  beforeEach(() => {
    // Intercept and mock API queries
    cy.intercept('GET', '/mainapi/admin/users', {
      statusCode: 200,
      body: [
        {
          id: 4,
          email: 'user2@example.com',
          pseudo: 'UserTwo',
          profilePicture: null,
        },
        {
          id: 5,
          email: 'user3@example.com',
          pseudo: 'UserThree',
          profilePicture: null,
        },
        {
          id: 6,
          email: 'user4@example.com',
          pseudo: 'UserFour',
          profilePicture: null,
        },
        {
          id: 7,
          email: 'user5@example.com',
          pseudo: 'UserFive',
          profilePicture: null,
        },
        {
          id: 8,
          email: 'user6@example.com',
          pseudo: 'UserSix',
          profilePicture: null,
        },
        {
          id: 9,
          email: 'user7@example.com',
          pseudo: 'UserNine',
          profilePicture: null,
        },
      ],
    }).as('usersRequest');
    cy.intercept('POST', '/authapi/admin/login', {
      statusCode: 200,
      body: {
        token: 'votre_token_simulé',
      },
    }).as('loginRequest');

    // Init params
    cy.viewport(1920, 1080);
    cy.visit('http://localhost:9000');

    cy.get('[test-id=input-email]')
      .type('admin1@example.com')
      .should('have.value', 'admin1@example.com');

    // Test login
    cy.get('[test-id=input-password]')
      .type('hashed_password1')
      .should('have.value', 'hashed_password1');
    cy.get('[test-id=button-submit-login').click();
    cy.url().should('include', '/users');

    // Test token
    cy.getCookie('token').should('exist');
    cy.wait('@loginRequest').then((interception) => {
      if (interception.response) {
        // Vérifie le statut de la réponse
        expect(interception.response.statusCode).to.eq(200);
        // Vérifie que le corps de la réponse contient un token non nul
        expect(interception.response.body.token).to.not.be.null;
      }
    });
  });

  it('Test menu-drawer', () => {
    // Test menu drawer
    cy.contains('Menu').should('be.visible');
    cy.get('#btn-toolbar').click();
    cy.contains('Menu').should('not.be.visible');
    cy.get('#btn-toolbar').click();
    cy.contains('Menu').should('be.visible');
  });

  it('Test Users', () => {
    // Intercept and mock API queries
    // cy.intercept('GET', '/mainapi/admin/images/*', {
    //   statusCode: 200,
    //   body: {
    //     size: 0,
    //   },
    // }).as('imageRequest');

    // Test link
    cy.contains('Utilisateurs').click();
    cy.url().should('include', '/users');

    // Test title
    cy.get('.q-table__title').contains('Utilisateurs');

    // Test table
    cy.get('.q-table').should('exist');

    // Test delete button tooltip
    cy.get('.delete-btn').eq(2).trigger('mouseenter');
    cy.get('.delete-tooltip').should('exist');
    cy.get('.delete-btn').eq(2).trigger('mouseleave');
    cy.get('.delete-tooltip').should('not.exist');

    // Test ban button tooltip
    cy.get('.ban-btn').eq(2).trigger('mouseenter');
    cy.get('.ban-tooltip').should('exist');
    cy.get('.ban-btn').eq(2).trigger('mouseleave');
    cy.get('.ban-tooltip').should('not.exist');

    // Test dialog doesn't exist before being called
    cy.get('.q-dialog').should('not.exist');

    // Test delete dialog
    cy.get('.delete-btn').eq(2).click();
    cy.get('.q-dialog').should('exist');
    cy.get('.q-dialog .q-dialog__title').should('contain', 'Confirm');
    cy.get('#cancel-dialog-btn').click();

    // Test ban dialog
    cy.get('.ban-btn').eq(2).click();
    cy.get('.q-dialog').should('exist');
    cy.get('.q-dialog .q-dialog__title').should('contain', 'Confirm');
    cy.get('#cancel-dialog-btn').click();

    // Test search input
    cy.get('.input-search input')
      .type('UserNine', { release: false })
      .should('have.value', 'UserNine');
    cy.get('.q-table').should(($table) => {
      expect($table.text()).to.contain('UserNine');
    });
  });
  it('Test botanists', () => {
    // Intercept and mock API queries
    cy.intercept('GET', '/mainapi/admin/botanists', {
      statusCode: 200,
      body: [
        {
          id: 4,
          email: 'user2@example.com',
          pseudo: 'UserTwo',
          profilePicture: null,
        },
        {
          id: 5,
          email: 'user3@example.com',
          pseudo: 'UserThree',
          profilePicture: null,
        },
        {
          id: 6,
          email: 'user4@example.com',
          pseudo: 'UserFour',
          profilePicture: null,
        },
        {
          id: 7,
          email: 'user5@example.com',
          pseudo: 'UserFive',
          profilePicture: null,
        },
        {
          id: 8,
          email: 'user6@example.com',
          pseudo: 'UserSix',
          profilePicture: null,
        },
        {
          id: 9,
          email: 'user7@example.com',
          pseudo: 'BotanistNine',
          profilePicture: null,
        },
      ],
    }).as('botanistsRequest');

    // Test link
    cy.contains('Botanistes').click();
    cy.url().should('include', '/botanists');

    // Test title
    cy.get('.q-table__title').contains('Botanistes');

    // Test table
    cy.get('.q-table').should('exist');

    // Test tooltip
    cy.get('.delete-btn').eq(2).trigger('mouseenter');
    cy.get('.delete-tooltip').should('exist');
    cy.get('.delete-btn').eq(2).trigger('mouseleave');
    cy.get('.delete-tooltip').should('not.exist');

    // Test tooltip
    cy.get('.ban-btn').eq(2).trigger('mouseenter');
    cy.get('.ban-tooltip').should('exist');
    cy.get('.ban-btn').eq(2).trigger('mouseleave');
    cy.get('.ban-tooltip').should('not.exist');

    // Test dialog doesn't exist before being called
    cy.get('.q-dialog').should('not.exist');

    // Test delete dialog
    cy.get('.delete-btn').eq(2).click();
    cy.get('.q-dialog').should('exist');
    cy.get('.q-dialog .q-dialog__title').should('contain', 'Confirm');
    cy.get('#cancel-dialog-btn').click();

    // Test ban dialog
    cy.get('.ban-btn').eq(2).click();
    cy.get('.q-dialog').should('exist');
    cy.get('.q-dialog .q-dialog__title').should('contain', 'Confirm');
    cy.get('#cancel-dialog-btn').click();

    // Test status toggle
    cy.get('.q-toggle__label')
      .eq(2)
      .then(($label) => {
        // cy.intercept('GET', '/mainapi/admin/botanists', {
        //   statusCode: 200,
        //   body: [
        //     {
        //       id: 4,
        //       email: 'user2@example.com',
        //       pseudo: 'UserTwo',
        //       profilePicture: null,
        //       validated: true,
        //     },
        //     {
        //       id: 5,
        //       email: 'user3@example.com',
        //       pseudo: 'UserThree',
        //       profilePicture: null,
        //       validated: false,
        //     },
        //     {
        //       id: 6,
        //       email: 'user4@example.com',
        //       pseudo: 'UserFour',
        //       profilePicture: null,
        //       validated: true,
        //     },
        //     {
        //       id: 7,
        //       email: 'user5@example.com',
        //       pseudo: 'UserFive',
        //       profilePicture: null,
        //       validated: false,
        //     },
        //     {
        //       id: 8,
        //       email: 'user6@example.com',
        //       pseudo: 'UserSix',
        //       profilePicture: null,
        //       validated: true,
        //     },
        //     {
        //       id: 9,
        //       email: 'user7@example.com',
        //       pseudo: 'BotanistNine',
        //       profilePicture: null,
        //       validated: null,
        //     },
        //   ],
        // }).as('botanistsRequest');
        // cy.get('.status-toggle').eq(2).click();
        // cy.get('.q-toggle__label').eq(2).contains('Accepté');

        // cy.intercept('GET', '/mainapi/admin/botanists', {
        //   statusCode: 200,
        //   body: [
        //     {
        //       id: 4,
        //       email: 'user2@example.com',
        //       pseudo: 'UserTwo',
        //       profilePicture: null,
        //       validated: true,
        //     },
        //     {
        //       id: 5,
        //       email: 'user3@example.com',
        //       pseudo: 'UserThree',
        //       profilePicture: null,
        //       validated: false,
        //     },
        //     {
        //       id: 6,
        //       email: 'user4@example.com',
        //       pseudo: 'UserFour',
        //       profilePicture: null,
        //       validated: false,
        //     },
        //     {
        //       id: 7,
        //       email: 'user5@example.com',
        //       pseudo: 'UserFive',
        //       profilePicture: null,
        //       validated: false,
        //     },
        //     {
        //       id: 8,
        //       email: 'user6@example.com',
        //       pseudo: 'UserSix',
        //       profilePicture: null,
        //       validated: true,
        //     },
        //     {
        //       id: 9,
        //       email: 'user7@example.com',
        //       pseudo: 'BotanistNine',
        //       profilePicture: null,
        //       validated: null,
        //     },
        //   ],
        // }).as('botanistsRequest');
        // cy.get('.status-toggle').eq(2).click();
        // cy.get('.q-toggle__label').eq(2).contains('Non accepté');

        if ($label.text().includes('Non accepté')) {
          cy.get('.status-toggle').eq(2).click();
          cy.get('.q-toggle__label').eq(2).contains('Accepté');
          cy.get('.status-toggle').eq(2).click();
          cy.get('.q-toggle__label').eq(2).contains('Non accepté');
        } else {
          cy.get('.status-toggle').eq(2).click();
          cy.get('.q-toggle__label').eq(2).contains('Non accepté');
          cy.get('.status-toggle').eq(2).click();
          cy.get('.q-toggle__label').eq(2).contains('Accepté');
        }
      });

    // Test search input
    cy.get('.input-search input')
      .type('BotanistNine', { release: false })
      .should('have.value', 'BotanistNine');
    cy.get('.q-table').should(($table) => {
      expect($table.text()).to.contain('BotanistNine');
    });
  });

  it('Test publications', () => {
    // Intercept and mock API queries
    cy.intercept('GET', '/mainapi/admin/users/4', {
      statusCode: 200,
      body: [
        {
          id: 4,
          email: 'user2@example.com',
          pseudo: 'UserTwo',
          profilePicture: null,
        },
      ],
    }).as('userRequest');
    cy.intercept('GET', '/mainapi/admin/publications/userGetAll?creatorId=4', {
      statusCode: 200,
      body: [
        {
          creatorId: 4,
          description: 'Je pars en vacance?',

          endingDate: '2200-03-23T21:51:12.8640628',
          id: 5,

          startingDate: '2024-03-16T21:51:12.8640628',

          status: false,
          title: 'Quelqu un pourrait arroser mon baobab?',
        },
      ],
    }).as('publicationsRequest');

    cy.contains('Utilisateurs').click();

    // Test tooltip
    cy.get('.publications-btn').eq(2).trigger('mouseenter');
    cy.get('.publications-tooltip').should('exist');
    cy.get('.publications-tooltip').contains('Voir publications');
    cy.get('.publications-btn').eq(2).trigger('mouseleave');
    cy.get('.publications-tooltip').should('not.exist');

    // Select the first <td> element in the Quasar table
    cy.get('.q-table tbody tr:first-child td:first-child')
      .invoke('text')
      .then((text) => {
        // Test URL
        cy.get('.publications-btn').eq(0).click();
        cy.url().should('include', '/users/' + text + '/posts');

        // Test title
        cy.contains('Publications Utilisateur');

        // Test table title and columns
        cy.get('.q-table__title').contains("Vue d'Ensemble");
        cy.get('.q-table').should('exist');
        cy.get('.q-table').contains('ID');
        cy.get('.q-table').contains('Début');
        cy.get('.q-table').contains('Fin');
        cy.get('.q-table').contains('Statut');
        cy.get('.q-table').contains('Titre');
        cy.get('.q-table').contains('Texte');

        // Test active-inactive toggle
        cy.get('.active-inactive-toggle').contains('En cours');
        cy.get('.active-inactive-toggle').click();
        cy.get('.active-inactive-toggle').contains('Passées');

        // Test back button
        cy.go('back');
        cy.url().should('include', '/users');
        cy.url().should('not.include', text + '/posts');
      });
  });
});

export {};
