import { Dialog, DialogChainObject } from 'quasar';

export abstract class DialogUtil {
  static createConfirmationDialog(message: string): DialogChainObject {
    return Dialog.create({
      title: 'Confirm',
      message: `${message}`,
      cancel: {
        text: 'white',
        color: '',
        id: 'cancel-dialog-btn',
      },
      ok: {
        text: 'white',
        color: '',
        id: 'ok-dialog-btn',
      },
      persistent: true,
    });
  }
}
export default DialogUtil;
