import { Notify } from 'quasar';

export abstract class NotificationUtil {
  static notifySuccess(message: string): void {
    Notify.create({
      message,
      type: 'positive',
      position: 'bottom',
      timeout: 2000,
      classes: 'notifySuccess',
    });
  }

  static notifyError(message: string): void {
    Notify.create({
      message,
      type: 'negative',
      position: 'bottom',
      timeout: 2000,
      classes: 'notifyError',
    });
  }

  // Ajoutez d'autres fonctions de notification au besoin
}
export default NotificationUtil;
