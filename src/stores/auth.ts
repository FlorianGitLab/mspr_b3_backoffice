import { defineStore } from 'pinia';

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    token: null as string | null,
  }),
  actions: {
    getToken() {
      return this.token;
    },
    setToken(token: string | null) {
      this.token = token;
    },
    clearToken() {
      this.token = null;
    },
  },
});
