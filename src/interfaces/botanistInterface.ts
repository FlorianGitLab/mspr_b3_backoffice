export interface BotanistInterface {
  id: number;
  pseudo: string;
  email: string;
  profilePicture: string;
  validated: boolean;
  comments: string;
}
