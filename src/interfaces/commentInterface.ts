export interface CommentInterface {
  id: number;
  creatorId: number;
  postTitle: string;
  postId: number;
  content: string;
  dateComment: Date;
}
