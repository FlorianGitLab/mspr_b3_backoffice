export interface PostInterface {
  creatorId: number;
  id: number;
  startingDate: Date;
  endingDate: Date;
  status: boolean;
  title: string;
  description: string;
}
