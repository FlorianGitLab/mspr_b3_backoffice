export interface UserInterface {
  id: number;
  pseudo: string;
  email: string;
  profilePicture: string;
  annonces: boolean;
}
