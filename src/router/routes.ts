import { Cookies } from 'quasar';
import {
  NavigationGuardNext,
  RouteLocationNormalized,
  RouteRecordRaw,
} from 'vue-router';

export function authGuard(
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
) {
  const token = Cookies.get('token');
  if (token) {
    // User is authenticated, proceed to the route
    next();
  } else {
    // User is not authenticated, redirect to login
    next('/login');
  }
}
// async function checkAuth(
//   to: RouteLocationNormalized,
//   from: RouteLocationNormalized,
//   next: NavigationGuardNext
// ) {
//   // Get the JWT token from cookies (assuming you are using a library like js-cookie)
//   const jwtToken = Cookies.get('your_jwt_cookie_name');

//   // Check if the token exists
//   if (!jwtToken) {
//     // Redirect to the login page if the token is not present
//     next('/login');
//   } else {
//     try {
//       // Validate the token by sending a request to your backend server
//       const response = await axios.post('/api/validate-token', {
//         token: jwtToken,
//       });

//       // Check the response status or any other validation logic based on your backend
//       if (response.status === 200) {
//         // Continue navigation if the token is valid
//         next();
//       } else {
//         // Redirect to the login page if the token is invalid
//         next('/login');
//       }
//     } catch (error) {
//       // Handle any errors that may occur during token validation
//       console.error('Error validating token:', error);
//       next('/login');
//     }
//   }
// }

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/login', // Redirige '/' vers '/users'
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: 'login', // Chemin pour la page de connexion
        name: 'login',
        component: () => import('pages/auth/loginPage.vue'),
      },
      {
        path: 'users',
        name: 'Users',
        component: () => import('pages/user/UsersListPage.vue'),
        beforeEnter: authGuard,
      },
      {
        path: 'users/:id',
        name: 'UserEdit',
        component: () => import('pages/user/UserEditPage.vue'),
        beforeEnter: authGuard, // Utilisation de la fonction de garde de navigation
      },
      {
        path: 'botanists',
        name: 'Botanists',
        component: () => import('pages/botanist/BotanistsListPage.vue'),
        beforeEnter: authGuard, // Utilisation de la fonction de garde de navigation
      },
      {
        path: 'botanists/:id',
        name: 'BotanistEdit',
        component: () => import('pages/botanist/BotanistEditPage.vue'),
        beforeEnter: authGuard, // Utilisation de la fonction de garde de navigation
      },
      {
        path: 'botanists/:id/comments',
        name: 'BotanistComments',
        component: () => import('pages/botanist/BotanistCommentsPage.vue'),
      },
      {
        path: 'users/:id/posts',
        name: 'UserPosts',
        component: () => import('pages/user/UserPostsPage.vue'),
      },
      {
        path: '/users/:id/posts/:postId',
        name: 'EditPost',
        component: () => import('pages/user/PostEditPage.vue'),
      },
      {
        path: '/botanists/:id/comments/:commentId',
        name: 'EditComment',
        component: () => import('pages/botanist/CommentEditPage.vue'),
      },
    ],
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
