import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';
import { TokenInterface } from 'src/interfaces/tokenInterface';
import CryptoJS from 'crypto-js';
// import { useAuthStore } from 'src/stores/auth';

export abstract class AuthService {
  private static tokenKey = 'token';
  private static AuthAxios: AxiosInstance = Axios.create({
    baseURL: '/authapi',
  });

  static async login(email: string, password: string) {
    try {
      const hashedPassword = CryptoJS.SHA256(password).toString();
      const response: AxiosResponse<TokenInterface> =
        await this.AuthAxios.post('/admin/login', {
          email: email,
          password: hashedPassword,
        });

      const token = response.data.token;

      Cookies.set(this.tokenKey, token, { expires: 1 });

    } catch (error) {
      console.error('Error fetching botanists:', error);
      throw error;
    }
  }

  static logout(): void {
    Cookies.remove(this.tokenKey);
  }

  static isAuthenticated(): boolean {
    const token = Cookies.get(this.tokenKey);
    return !!token;
  }
}

export default AuthService;
