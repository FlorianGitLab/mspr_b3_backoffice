import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';
import { BotanistInterface } from 'src/interfaces/botanistInterface';
import { imageService } from './imageService';

export abstract class BotanistService {
  private static botanistAxios: AxiosInstance = Axios.create({
    baseURL: '/mainapi',
  });

  static async getBotanists(): Promise<BotanistInterface[]> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      const response: AxiosResponse<BotanistInterface[]> =
        await this.botanistAxios.get('admin/botanists', config);

      for (const botanist of response.data) {
        if (botanist.profilePicture) {
          botanist.profilePicture = await imageService.getImage(
            botanist.profilePicture
          );
        }
      }

      return response.data;
    } catch (error) {
      console.error('Error fetching botanists:', error);
      throw error;
    }
  }

  static async getBotanistById(idBotanist: number): Promise<BotanistInterface> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      const response: AxiosResponse<BotanistInterface> = await this.botanistAxios.get(`admin/botanists/${idBotanist}`, config);

      if (response.data.profilePicture) {
        response.data.profilePicture = await imageService.getImage(response.data.profilePicture);
      }

      return response.data;
    } catch (error) {
      console.error(`Error fetching botanist with id ${idBotanist}:`, error);
      throw error;
    }
  }

  static async acceptStatus(idBotanist: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.botanistAxios.patch(
        `admin/botanists/${idBotanist}/accept`,
        {},
        config
      );
      // this.getBotanists();
    } catch (error) {
      console.error(
        `Error accepting status for botanist with id ${idBotanist}:`,
        error
      );
      throw error;
    }
  }

  static async refuseStatus(idBotanist: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.botanistAxios.patch(
        `admin/botanists/${idBotanist}/refuse`,
        {},
        config
      );
    } catch (error) {
      console.error(
        `Error not accepting status for botanist with id ${idBotanist}:`,
        error
      );
      throw error;
    }
  }

  // static async pendingStatus(idBotanist: number): Promise<void> {
  //   try {
  //     const endpoint = `/botanists/${idBotanist}/pending`;
  //     await this.botanistAxios.patch(endpoint);
  //     this.getBotanists();
  //   } catch (error) {
  //     console.error(
  //       `Error refusing status for botanist with id ${idBotanist}:`,
  //       error
  //     );
  //     throw error;
  //   }
  // }

  static async deletePhoto(idBotanist: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.botanistAxios.patch(
        `admin/botanists/${idBotanist}/picture/remove`,
        {},
        config
      );
    } catch (error) {
      console.error(
        `Error removing picture for botanist with id ${idBotanist}:`,
        error
      );
      throw error;
    }
  }

  static async banBotanist(idBotanist: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.botanistAxios.delete(
        `admin/botanists/${idBotanist}/ban`,
        config
      );
    } catch (error) {
      console.error(`Error ban botanist with id ${idBotanist}:`, error);
      throw error;
    }
  }

  static async deleteBotanist(idBotanist: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.botanistAxios.delete(
        `admin/botanists/${idBotanist}/delete`,
        config
      );
    } catch (error) {
      console.error(`Error delete botanist with id ${idBotanist}:`, error);
      throw error;
    }
  }
}

export default BotanistService;
