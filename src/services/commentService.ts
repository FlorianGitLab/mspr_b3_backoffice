import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';
import { CommentInterface } from 'src/interfaces/commentInterface';

export abstract class commentService {
  private static postAxios: AxiosInstance = Axios.create({
    baseURL: '/mainapi',
  });

  static async getCommentById(idComment: number): Promise<CommentInterface> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      const response: AxiosResponse<CommentInterface> = await this.postAxios.get(`/admin/comments/${idComment}`, config);
      return response.data;
    } catch (error) {
      console.error(`Error fetching comment with id ${idComment}:`, error);
      throw error;
    }
  }

  static async getBotanistComments(idBotanist: number): Promise<CommentInterface[]> {
    const token = Cookies.get('token');
    const config = {
      headers: {
        Authorization: ` ${token}`,
      },
    };

    try {
      const response: AxiosResponse<CommentInterface[]> = await this.postAxios.get(`/admin/comments?creatorId=${idBotanist}`, config);
      return response.data;
    } catch (error) {
      console.error('Error fetching comments:', error);
      throw error;
    }
  }

  static async deleteComment(idComment: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      await this.postAxios.delete(`/admin/comments/${idComment}`, config);
    } catch (error) {
      console.error(`Error deleting comment with id ${idComment}:`, error);
      throw error;
    }
  }

  static async editComment(idComment: number, updatedData: object): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      await this.postAxios.patch(`/admin/comments/${idComment}`, updatedData, config);
    } catch (error) {
      console.error(`Error editing comment with id ${idComment}:`, error);
      throw error;
    }
  }
}

export default commentService;
