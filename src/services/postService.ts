import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';
import { PostInterface } from 'src/interfaces/postInterface';

export abstract class postService {
  private static postAxios: AxiosInstance = Axios.create({
    baseURL: '/mainapi',
  });

  static async getPostById(idPost: number): Promise<PostInterface> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      const response: AxiosResponse<PostInterface> = await this.postAxios.get(
        `admin/publications/${idPost}`,
        config
      );
      return response.data;
    } catch (error) {
      console.error(`Error fetching post with id ${idPost}:`, error);
      throw error;
    }
  }

  static async getUserPosts(idUser: number): Promise<PostInterface[]> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      const response: AxiosResponse<PostInterface[]> = await this.postAxios.get(
        `admin/publications/userGetAll?creatorId=${idUser}`,
        config
      );

      return response.data;
    } catch (error) {
      console.error('Error fetching posts:', error);
      throw error;
    }
  }

  static async deletePost(idPost: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      await this.postAxios.delete(`admin/publications/${idPost}/delete`, config);
    } catch (error) {
      console.error(`Error deleting post with id ${idPost}:`, error);
      throw error;
    }
  }

  static async editPost(idPost: number, updatedData: object): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      await this.postAxios.patch(
        `admin/publications/${idPost}/edit`,
        updatedData,
        config
      );
    } catch (error) {
      console.error(`Error editing post with id ${idPost}:`, error);
      throw error;
    }
  }
}

export default postService;
