import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';
import { UserInterface } from 'src/interfaces/userInterface';
import { imageService } from './imageService';

export abstract class userService {
  private static userAxios: AxiosInstance = Axios.create({
    baseURL: '/mainapi',
  });

  static async getUsers(): Promise<UserInterface[]> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: {
          Authorization: ` ${token}`,
        },
      };

      const response: AxiosResponse<UserInterface[]> = await this.userAxios.get(
        '/admin/users',
        config
      );

      for (const user of response.data) {
        if (user.profilePicture) {
          user.profilePicture = await imageService.getImage(user.profilePicture);
        }
      }

      return response.data;
    } catch (error) {
      console.error('Error fetching users:', error);
      throw error;
    }
  }

  static async getUserById(idUser: number): Promise<UserInterface> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      const response: AxiosResponse<UserInterface> = await this.userAxios.get(`admin/users/${idUser}`, config);

      if (response.data.profilePicture) {
        response.data.profilePicture = await imageService.getImage(response.data.profilePicture);
      }

      return response.data;
    } catch (error) {
      console.error(`Error fetching user with id ${idUser}:`, error);
      throw error;
    }
  }

  static async deletePhoto(idUser: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.userAxios.patch(
        `admin/botanists/${idUser}/picture/remove`,
        {},
        config
      );
    } catch (error) {
      console.error(
        `Error removing picture for botanist with id ${idUser}:`,
        error
      );
      throw error;
    }
  }

  static async banUser(idUser: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.userAxios.delete(
        `admin/users/${idUser}/ban`,
        config
      );
    } catch (error) {
      console.error(`Error ban user with id ${idUser}:`, error);
      throw error;
    }
  }

  static async deleteUser(idUser: number): Promise<void> {
    try {
      const token = Cookies.get('token');
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      await this.userAxios.delete(
        `admin/users/${idUser}/delete`,
        config
      );
    } catch (error) {
      console.error(`Error delete botanist with id ${idUser}:`, error);
      throw error;
    }
  }
}

export default userService;
