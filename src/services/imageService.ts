import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import { Cookies } from 'quasar';

export abstract class imageService {
  private static userAxios: AxiosInstance = Axios.create({
    baseURL: '/mainapi',
  });

  static async getImage(image: string): Promise<string> {
    try {
      const token = Cookies.get('token');
      const response: AxiosResponse<Blob> = await this.userAxios.get(
        `/images/${image}`,
        { responseType: 'blob', headers: { Authorization: `Bearer ${token}` } }
      );
      const blobUrl = URL.createObjectURL(response.data);
      return blobUrl;
    } catch (error) {
      console.error('Error fetching user image:', error);
      throw error;
    }
  }
}
